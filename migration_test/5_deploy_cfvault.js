const AddressArray = artifacts.require("AddressArray");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20DepositApprover = artifacts.require("ERC20DepositApprover");
const SafeERC20 = artifacts.require("SafeERC20");
const SafeMath = artifacts.require("SafeMath");
const {DHelper, StepRecorder} = require("./util.js");
const DummyCFVault = artifacts.require("DummyCFVault");
const USDC = artifacts.require("USDC");
const TransferableToken = artifacts.require("TransferableToken");
const DummyCFVaultFactory = artifacts.require("DummyCFVaultFactory");
const UpgradeV1ToV2 = artifacts.require("UpgradeV1ToV2");


async function performMigration(deployer, network, accounts) {
  await AddressArray.deployed();
  await SafeERC20.deployed();
  await SafeMath.deployed();
  await TransferableToken.deployed();

  tlistfactory = await TrustListFactory.deployed();
  tx = await tlistfactory.createTrustList(['0x0000000000000000000000000000000000000000']);
  tlist = await TrustList.at(tx.logs[0].args.addr);

  await deployer.link(TransferableToken, DummyCFVaultFactory);
  await deployer.link(SafeMath, DummyCFVaultFactory);
  await deployer.deploy(DummyCFVaultFactory);
  dcf = await DummyCFVaultFactory.deployed()

  sr = new StepRecorder(network, "cfvault");

  tf = await ERC20TokenFactory.deployed();

  await deployer.deploy(USDC);
  usdc = await USDC.deployed();
  sr.write("usdc", usdc.address);

  tx = await tf.createCloneToken('0x0000000000000000000000000000000000000000', 0, "ENF_vUSDC", 18, "ENF_vUSDC", true, tlist.address);
  v1_lp = await ERC20Token.at(tx.logs[0].args._cloneToken);
  sr.write("v1-lp", v1_lp.address);
  tx = await dcf.createCFVault(usdc.address, v1_lp.address);
  v1_vault = await DummyCFVault.at(tx.logs[0].args.addr);
  sr.write("v1-vault", v1_vault.address)
  await tlist.add_trusted(v1_vault.address);

  tx = await tf.createCloneToken('0x0000000000000000000000000000000000000000', 0, "ENF_vUSDC", 18, "ENF_vUSDC", true, tlist.address);
  v2_lp = await ERC20Token.at(tx.logs[0].args._cloneToken);
  sr.write("v2-lp", v2_lp.address);
  tx = await dcf.createCFVault(usdc.address, v3_lp.address);
  v2_vault = await DummyCFVault.at(tx.logs[0].args.addr);
  sr.write("v2-vault", v2_vault.address)
  await tlist.add_trusted(v2_vault.address);

  await deployer.link(TransferableToken,UpgradeV1ToV2);
  await deployer.deploy(UpgradeV1ToV2, v1_vault.address, v2_vault.address);
  u = await UpgradeV1ToV2.deployed();
  sr.write("upgrade", u.address);
  await tlist.add_trusted(u.address);

}

module.exports = function(deployer, network, accounts){
deployer
    .then(function() {
      return performMigration(deployer, network, accounts)
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
