const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const TransferableToken = artifacts.require("TransferableToken");

module.exports = function(deployer) {
  deployer.deploy(SafeMath);
  deployer.deploy(AddressArray);
  deployer.deploy(Address);
  deployer.deploy(SafeERC20);

  deployer.deploy(TransferableToken);
};
