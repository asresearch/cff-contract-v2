const {DHelper, StepRecorder} = require("./util.js");
const AavePool = artifacts.require("AavePool")
const LusdPool = artifacts.require("LusdPool")
const AlethPool = artifacts.require("AlethPool")

async function performMigration(deployer, network, accounts, dhelper) {
  owner = "0x66945268093f28c28A2dcE11a1640e06c070df7B"

  usdc_ctrl = "0x73B1502149F4296F137F3d432C74e62344CfC2d7"
  usdc_vault = "0x889B9194Fb1D66509d3d043e7c839582fED6E607"
  aave = await dhelper.readOrCreateContract(AavePool)
  await aave.setController(usdc_ctrl, usdc_vault)
  await aave.transferOwnership(owner)

  lusd = await dhelper.readOrCreateContract(LusdPool)
  await lusd.setController(usdc_ctrl, usdc_vault)
  await lusd.transferOwnership(owner)

  eth_ctrl = "0x166Ba970F58A2fEbDbe0280B720862154453e645"
  eth_vault = "0x42c5a43DE403A1ed504ceE2FE552B3fCE2b19b9a"
  aleth = await dhelper.readOrCreateContract(AlethPool)
  await aleth.setController(eth_ctrl, eth_vault)
  await aleth.transferOwnership(owner)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
