import pytest
from brownie import *


def log(text, desc=''):
    print('\033[32m' + text + '\033[0m' + desc)
# deploy safemath


@pytest.fixture
def safemath():
    owner = accounts[0]
    SafeMath.deploy({"from": owner})


@pytest.fixture
def transferableToken():
    owner = accounts[0]
    s = TransferableToken.deploy({"from": owner})
    return s


# deploy AddressArray


@pytest.fixture()
def addressArray():
    owner = accounts[0]
    a = AddressArray.deploy({"from": owner})
    return a
# deploy hack contract


@pytest.fixture()
def hack(Vault, compoundPool, LPERC20):
    hacker = accounts[1]
    cEther = "0x4ddc2d193948926d02f9b1fe9e1daa0718270ed5"
    cWBTC = '0xC11b1268C1A384e55C48c2391d8d480264A3A7F4'
    WBTC = "0x2260fac5e5542a773aa44fbcfedf7c193bc2c599"
    ctrl = "0x3d9819210a31b4961b30ef54be2aed79b9c9cd3b"
    c = CFFHack.deploy(cEther, cWBTC, WBTC, ctrl, Vault.address,
                       compoundPool.address, LPERC20.address, {"from": hacker})
    # mock initial balance
    hacker.transfer(c.address, '5 ether')
    return c


def hackForERC20(Pool, lp, vault):
    owner = accounts[0]
    hacker = accounts[0]
    cEther = "0x4ddc2d193948926d02f9b1fe9e1daa0718270ed5"
    cWBTC = '0xC11b1268C1A384e55C48c2391d8d480264A3A7F4'
    WBTC = "0x2260fac5e5542a773aa44fbcfedf7c193bc2c599"
    ctrl = "0x3d9819210a31b4961b30ef54be2aed79b9c9cd3b"
    c = CFFHack.deploy(cEther, cWBTC, WBTC, ctrl, vault.address,
                       Pool.address, lp.address, {"from": hacker})
    return c

# deploy ETH vault


@pytest.fixture()
def Vault(LPERC20, controller):
    owner = accounts[0]
    targetToken = "0x2260fac5e5542a773aa44fbcfedf7c193bc2c599"

    v = CFVaultV2.deploy(targetToken, LPERC20, controller, {"from": owner})
    controller.setVault(v)
    return v

# deploy trustlist


@pytest.fixture()
def trustList(addressArray):
    owner = accounts[0]
    t = TrustList.deploy([], {"from": owner})
    return t


@pytest.fixture()
def exchange():
    crv = "0xD533a949740bb3306d119CC777fa900bA034cd52"
    owner = accounts[0]

    e = CRVExchangeV2.deploy(crv, {"from": owner})

    e.addPath("0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F", ["0xD533a949740bb3306d119CC777fa900bA034cd52",
                                                             "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2", "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48"])  # Add CRV->WETH->USDC Path
    e.addPath("0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F", ["0xD533a949740bb3306d119CC777fa900bA034cd52",
                                                             "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2", "0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599"])  # Add CRV->WETH->WBTC Path

    return e

# deploy ERC20 factory


@pytest.fixture()
def LPERC20(trustList):
    owner = accounts[0]
    e = ERC20Token.deploy("0x0000000000000000000000000000000000000000", "0x0000000000000000000000000000000000000000",
                          0, "USDCLP", 18, "ULP", True, trustList.address, {"from": owner})
    return e


@pytest.fixture()
def controller(safemath, addressArray, exchange, transferableToken):
    crv = "0xD533a949740bb3306d119CC777fa900bA034cd52"
    target = "0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599"  # wbtc
    owner = accounts[0]

    c = CFControllerV2.deploy(crv, target, 1, {"from": owner})
    c.changeYieldHandler(exchange)
    return c


@pytest.fixture()
def hbtcPool(safemath):
    owner = accounts[0]
    c = HbtcPool.deploy({"from": owner})
    return c


@pytest.fixture()
def bbtcPool(safemath):
    owner = accounts[0]
    c = BbtcPool.deploy({"from": owner})
    return c


@pytest.fixture()
def tbtcPool(safemath):
    owner = accounts[0]
    c = TbtcPool.deploy({"from": owner})
    return c


@pytest.fixture()
def renbtcPool(safemath):
    owner = accounts[0]
    return RenbtcPool.deploy({"from": owner})


@pytest.fixture()
def sbtcPool(safemath):
    return SbtcPool.deploy({"from": accounts[0]})


def withdrawLP(hackContract, LPERC20):
    LPBalance = LPERC20.balanceOf(hackContract.address)
    USDCBalanceBefore = hackContract.getDAIBalance()
    log("Balance before withdraw ", str(USDCBalanceBefore))
    halfLP = LPBalance /2

    tx = hackContract.withdraw(halfLP)
    tx.wait(1)
    LPBalance = LPERC20.balanceOf(hackContract.address)
    tx = hackContract.withdraw(LPBalance)
    tx.wait(1)

    USDCBalanceAfter = hackContract.getDAIBalance()
    log("Balance after withdraw ", str(USDCBalanceAfter))

# # # ------------------------hbtcPool Test----------------------------------------
def test_hbtcPool(controller, hbtcPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(hbtcPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(hbtcPool.address)

    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Gusd Pool
    hbtcPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    # hackContract.mintAndLoan(5000000, {"from": hacker})
    hackContract.swap("0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F",
                      ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
                       "0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599"],
                      5000000)
    vaultAddress = hackContract.cfVault()
    assert vaultAddress == Vault.address
    log("-----------Testing hBtc Deposit------------")
    log(" HbtcPool WBTC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)


# # ------------------------tbtcPool Test----------------------------------------
def test_tbtcPool(controller, tbtcPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(tbtcPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(tbtcPool.address)

    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Gusd Pool
    tbtcPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    # hackContract.mintAndLoan(5000000, {"from": hacker})
    hackContract.swap("0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F",
                      ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
                       "0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599"],
                      5000000)
    vaultAddress = hackContract.cfVault()
    assert vaultAddress == Vault.address
    log("-----------Testing tBtc Deposit------------")
    log("tbtcPool Pool WBTC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)

# # ------------------------bbtcPool Test----------------------------------------


def test_bbtcPool(controller, bbtcPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(bbtcPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(bbtcPool.address)

    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Gusd Pool
    bbtcPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    # hackContract.mintAndLoan(5000000, {"from": hacker})
    hackContract.swap("0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F",
                      ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
                       "0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599"],
                      5000000)
    vaultAddress = hackContract.cfVault()
    assert vaultAddress == Vault.address
    log("-----------Testing bBtc Deposit------------")
    log("bbtcPool Pool WBTC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)

# # ------------------------renbtcPool Test----------------------------------------


def test_renbtcPool(controller, renbtcPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(renbtcPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(renbtcPool.address)

    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Gusd Pool
    renbtcPool.setController(controller.address, Vault.address)

    log("-----------Testing renbtc Deposit------------")

    # mint USDC for prepare
    hackContract.swap(
        "0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F",
        ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
            "0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599"],  # ETH -> wbtc
        5000000
    )

    log("renbtcPool Pool WBTC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("WBTC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))

    log("-----------Earning Farm----------------------")
    log("asset before: ", str(Vault.get_asset()))
    chain.mine(5760)
    chain.sleep(86400)

    controller.earnReward(1)
    log("asset after : ", str(Vault.get_asset()))

    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)


# # ------------------------sbtcPool Test----------------------------------------
def test_sbtcPool(controller, sbtcPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(sbtcPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(sbtcPool.address)

    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Gusd Pool
    sbtcPool.setController(controller.address, Vault.address)

    log("-----------Testing sbtc Deposit------------")

    # mint USDC for prepare
    hackContract.swap(
        "0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F",
        ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
            "0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599"],  # ETH -> wbtc
        5000000
    )

    log("sbtcPool Pool WBTC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("WBTC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))

    log("-----------Earning Farm----------------------")
    log("asset before: ", str(Vault.get_asset()))
    chain.mine(576)
    chain.sleep(86400)

    controller.earnReward(1)
    log("asset after : ", str(Vault.get_asset()))

    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)
