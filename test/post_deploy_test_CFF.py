import pytest
from brownie import *
import os
import json

cur_dir = os.path.dirname(os.path.abspath(__file__))


def log(text, desc=''):
    print('\033[32m' + text + '\033[0m' + desc)


def read_contract_addr(key):
    fp = os.path.join(cur_dir, "../main-common.json")
    with open(fp) as of:
        r = json.load(of)
        t = r['main']
        if key in t:
            return t[key]
    fp = os.path.join(cur_dir, "../main-USD Coin.json")

    with open(fp) as of:
        r = json.load(of)
        t = r['main']
        if key in t:
            return t[key]

    raise RuntimeError("invalid key {}".format(key))


def read_abi(c):
    fp = os.path.join(cur_dir, "../build/contracts/{}.json".format(c))
    with open(fp) as of:
        r = json.load(of)
        return r['abi']


def contract(name, addr):
    return Contract.from_abi(name, addr, read_abi(name))


def hackForERC20(lp, vault):
    owner = accounts[0]
    hacker = accounts[0]
    cEther = "0x4ddc2d193948926d02f9b1fe9e1daa0718270ed5"
    cUSDC = '0x39aa39c021dfbae8fac545936693ac917d5e7563'
    USDC = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48"
    ctrl = "0x3d9819210a31b4961b30ef54be2aed79b9c9cd3b"
    nouse = ctrl
    c = CFFHack.deploy(cEther, cUSDC, USDC, ctrl, vault.address,
                       nouse, lp.address, {"from": hacker})
    return c

# deploy ETH vault


def withdrawLP(hackContract, LPERC20):
    LPBalance = LPERC20.balanceOf(hackContract.address)
    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    halfLP = LPBalance / 2

    tx = hackContract.withdraw(halfLP)
    tx.wait(1)
    LPBalance = LPERC20.balanceOf(hackContract.address)
    tx = hackContract.withdraw(LPBalance)
    tx.wait(1)

    USDCBalanceAfter = hackContract.getDAIBalance()
    log("USDC Balance after withdraw ", str(USDCBalanceAfter))


def call_pool():
    controller = contract(
        "CFControllerV2", read_contract_addr("CF controller"))
    vault = contract("CFVaultV2", read_contract_addr("CF vault"))
    lp = contract("ERC20Token", vault.lp_token())

    hacker = accounts[9]
    hackContract = hackForERC20(lp, vault)
    hacker.transfer(hackContract.address, '90 ether')
    hackContract.swap(
        "0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F",
        ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
            "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48"],
        100000000000
    )
    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before supply ", str(USDCBalanceBefore))
    log("Pool USDC deposit start")
    hackContract.supplyUSDC()

    LPBalance = lp.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))

    # log("-----------Earning Farm----------------------")
    # log("asset before: ", str(vault.get_asset()))
    # chain.mine(5760)
    # controller.earnReward(0)
    # log("asset after : ", str(vault.get_asset()))

    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, lp)


# --------------------------AlusdPool Test--------------------------------------
def test_AlusdPool():
    call_pool()

    '''
    owner = accounts[0]
    hacker = accounts[9]
    hackContract = hackForERC20(alusdPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '90 ether')
    # add pool to controller
    controller.add_pool(alusdPool.address)
    # controller.changeExtraToken("0x6DEA81C8171D0bA574754EF6F8b412F2Ed88c54D")

    # cfVault
    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Aave Pool
    alusdPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    hackContract.swap(
        "0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F",
        ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
            "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48"],
        100000000000
    )

    vaultAddress = hackContract.cfVault()
    #log("v1: ", vaultAddress, " should be ", hackContract.cfVault())
    assert vaultAddress == Vault.address
    log("-----------Testing AlusdPool Deposit------------")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before supply ", str(USDCBalanceBefore))
    log("alusdPool Pool USDC deposit start")
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))

    log("-----------Earning Farm----------------------")
    log("asset before: ", str(Vault.get_asset()))
    chain.mine(5760)
    controller.earnReward(0)
    log("asset after : ", str(Vault.get_asset()))

    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)
    '''


# --------------------------LusdPool Test--------------------------------------
def test_LusdPool(hack, controller, lusdPool, Vault, trustList, LPERC20):

    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(lusdPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '10 ether')
    # add pool to controller
    controller.add_pool(lusdPool.address)
    # controller.changeExtraToken("0x6DEA81C8171D0bA574754EF6F8b412F2Ed88c54D")

    # cfVault
    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Aave Pool
    lusdPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    hackContract.swap(
        "0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F",
        ["0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
            "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48"],
        10000000000
    )

    crv = "0xD533a949740bb3306d119CC777fa900bA034cd52"
    lqty = "0x6DEA81C8171D0bA574754EF6F8b412F2Ed88c54D"

    vaultAddress = hackContract.cfVault()
    #log("v1: ", vaultAddress, " should be ", hackContract.cfVault())
    assert vaultAddress == Vault.address
    log("-----------Testing LusdPool Deposit------------")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before supply ", str(USDCBalanceBefore))
    log("lusdPool Pool USDC deposit start")
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))

    log("-----------Earning Farm----------------------")
    log("asset before: ", str(Vault.get_asset()))
    chain.mine(576)
    controller.earnReward(0)
    log("asset after : ", str(Vault.get_asset()))

    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)


# --------------------------CompoundPool Test--------------------------------------
def test_CompoundPool(hack, controller, compoundPool, Vault, trustList, LPERC20):

    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(compoundPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '10 ether')
    # add pool to controller
    controller.add_pool(compoundPool.address)

    # cfVault
    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Aave Pool
    compoundPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    hackContract.mintAndLoan(1000000000, {"from": hacker})

    crv = "0xD533a949740bb3306d119CC777fa900bA034cd52"

    vaultAddress = hackContract.cfVault()
    #log("v1: ", vaultAddress, " should be ", hackContract.cfVault())
    assert vaultAddress == Vault.address
    log("-----------Testing compoundPool Deposit------------")

    log("CRV balance at controller ", str(
        interface.IERC20(crv).balanceOf(controller.address)))
    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before supply ", str(USDCBalanceBefore))
    log("compoundPool Pool USDC deposit start")
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)

    controller.earnReward(0)
    USDCBalanceAfter = hackContract.getDAIBalance()
    log("USDC Balance after withdraw ", str(USDCBalanceAfter))


# --------------------------AavePool Test--------------------------------------
def test_AavePool(controller, aavePool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(aavePool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(aavePool.address)

    # cfVault
    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Aave Pool
    aavePool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    hackContract.mintAndLoan(1000000000, {"from": hacker})
    vaultAddress = hackContract.cfVault()
    assert vaultAddress == Vault.address
    log("-----------Testing AavePool Deposit------------")
    log("Aave Pool USDC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)

# # ------------------------BusdPool Test----------------------------------------


def test_BusdPool(controller, busdPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(busdPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(busdPool.address)

    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Busd Pool
    busdPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    hackContract.mintAndLoan(1000000000, {"from": hacker})
    vaultAddress = hackContract.cfVault()
    assert vaultAddress == Vault.address
    log("-----------Testing Busd Pool Deposit------------")
    log("busd Pool USDC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)


# # # ------------------------GusdPool Test----------------------------------------
def test_GusdPool(controller, gusdPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(gusdPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(gusdPool.address)
    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Gusd Pool
    gusdPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    hackContract.mintAndLoan(1000000000, {"from": hacker})
    vaultAddress = hackContract.cfVault()
    assert vaultAddress == Vault.address
    log("-----------Testing GUSD Deposit------------")
    log("gusd Pool USDC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)


# # ------------------------YPool Test----------------------------------------
def test_YPool(controller, yPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(yPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(yPool.address)

    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Gusd Pool
    yPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    hackContract.mintAndLoan(1000000000, {"from": hacker})
    vaultAddress = hackContract.cfVault()
    assert vaultAddress == Vault.address
    log("-----------Testing Ypool Deposit------------")
    log("Y Pool USDC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)


# # ------------------------TriPool Test----------------------------------------
def test_TriPool(controller, triPool, Vault, trustList, LPERC20):
    owner = accounts[0]
    hacker = accounts[1]
    hackContract = hackForERC20(triPool, LPERC20, Vault)
    # mock initial balance
    hacker.transfer(hackContract.address, '5 ether')
    # add pool to controller
    controller.add_pool(triPool.address)

    Vault.set_max_amount(
        int(99999999999999999999999999999999999999999999999999999999))
    Vault.set_slippage(9000)
    # add Vault address to trustlist
    trustList.add_trusted(Vault.address)
    # set controller and Vault address for Gusd Pool
    triPool.setController(controller.address, Vault.address)
    # mint USDC for prepare
    hackContract.mintAndLoan(1000000000, {"from": hacker})
    vaultAddress = hackContract.cfVault()
    assert vaultAddress == Vault.address
    log("-----------Testing TriPool Deposit------------")
    log("Tri Pool USDC deposit start")

    USDCBalanceBefore = hackContract.getDAIBalance()
    log("USDC Balance before withdraw ", str(USDCBalanceBefore))
    hackContract.supplyUSDC()
    LPBalance = LPERC20.balanceOf(hackContract.address)
    log("LP Gained: ", str(LPBalance))
    log("-----------Withdrawing LP----------------------")
    withdrawLP(hackContract, LPERC20)
