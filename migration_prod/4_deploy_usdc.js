const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20DepositApprover = artifacts.require("ERC20DepositApprover");
const TransferableToken = artifacts.require("TransferableToken")
const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const CFControllerV2 = artifacts.require("CFControllerV2")
const CFVaultV2 = artifacts.require("CFVaultV2")
const UpgradeV1ToV2 = artifacts.require("UpgradeV1ToV2")
const Ownable = artifacts.require("Ownable")
const {DHelper, StepRecorder} = require("./util.js");
//const {deploy_vault_and_controller, change_owner, deploy_pool, deploy_upgrade} = require("./common_deploy.js")
const AavePool = artifacts.require("AavePool")
const AlusdPoolV2 = artifacts.require("AlusdPoolV2")
const BUSDPool = artifacts.require("BUSDPool")
const CompoundPool = artifacts.require("CompoundPool")
const GUSDPool = artifacts.require("GUSDPool")
const TriPool = artifacts.require("TriPool")
const YPool = artifacts.require("YPool")

var get_target_name = async function(target_token){
  if(target_token == '0x0000000000000000000000000000000000000000')
    return 'eth'
  t = await ERC20Token.at(target_token)
  name = await t.name()
  return name
}
var deploy_vault_and_controller = async function(dhelper, target_token, lp_symbol, network){
  //usdc = "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48";
  name = await get_target_name(target_token)
  sr = new StepRecorder(network, name)
  sr.write("target token", target_token)
  sr.write('name', name)
  crv = "0xD533a949740bb3306d119CC777fa900bA034cd52";
  egap = 5760;

  result = {}
  tlistfactory = await dhelper.readOrCreateContract(TrustListFactory);
  tx = await tlistfactory.createTrustList(['0x0000000000000000000000000000000000000000']);
  tlist = await TrustList.at(tx.logs[0].args.addr);
  sr.write("trustlist", tlist.address)

  tf = await dhelper.readOrCreateContract(ERC20TokenFactory)
  tx = await tf.createCloneToken('0x0000000000000000000000000000000000000000', 0, lp_symbol + " v2", 18, lp_symbol, true, tlist.address);
  lp = await ERC20Token.at(tx.logs[0].args._cloneToken);
  sr.write("CF token", lp.address)

  controller = await dhelper.readOrCreateContract(CFControllerV2, [TransferableToken, AddressArray, SafeMath, Address], crv, target_token, egap)
  sr.write("CF controller", controller.address)

  vault = await dhelper.readOrCreateContract(CFVaultV2, [SafeMath, Address, TransferableToken], target_token, lp.address, controller.address)
  sr.write("CF vault", vault.address)
  await controller.setVault(vault.address)
  await tlist.add_trusted(vault.address)
  return {tlist, lp, vault, controller}
}

var deploy_upgrade = async function(network, dhelper, target_token, old_vault){
  name = await get_target_name(target_token)
  sr = new StepRecorder(network, name)
  nv = sr.value("CF vault")
  up = await dhelper.readOrCreateContract(UpgradeV1ToV2, [TransferableToken], old_vault, nv)
  sr.write("upgrade", up.address)
  sr.write("old vault", old_vault)
}

var deploy_pool = async function(network, dhelper, target_token, owner, pools){
  name = await get_target_name(target_token)
  sr = new StepRecorder(network, name)
  controller = await CFControllerV2.at(sr.value("CF controller"))
  vault = await CFVaultV2.at(sr.value("CF vault"))

  for(let c of pools){
    n = c._json.contractName
    ci = await dhelper.readOrCreateContract(c)
    sr.write(n, ci.address)
    await ci.setController(controller.address, vault.address)
    await controller.add_pool(ci.address)
    await ci.transferOwnership(owner)
  }
}

var change_owner = async function(network, target_token, owner){
  name = await get_target_name(target_token)
  sr = new StepRecorder(network, name)
  v = await Ownable.at(sr.value("trustlist"))
  v.transferOwnership(owner)

  v = await Ownable.at(sr.value("CF token"))
  v.transferOwnership(owner)

  v = await Ownable.at(sr.value("CF controller"))
  v.transferOwnership(owner)

  v = await Ownable.at(sr.value("CF vault"))
  v.transferOwnership(owner)
}
async function performMigration(deployer, network, accounts, dhelper) {
  usdc = "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48";
  lp_sym= "ENF_vUSDC"
  let {lptlist, lp, vault, controller} = await deploy_vault_and_controller(dhelper, usdc, lp_sym, network)
  fee = "0x39F4Ef6294512015AB54ed3ab32BAA1794E8dE70"
  sr = new StepRecorder(network, "common")
  exchange = sr.value("CRVExchange")
  await vault.set_slippage(9920);
  await vault.set_max_amount(10000000000000);
  await vault.changeWithdrawFee(10);
  await vault.changeFeePool(fee)
  await controller.changeYieldHandler(exchange)
  await controller.changeHarvestFee(1000);
  await controller.changeFeePool(fee);
  await controller.addExtraToken("0xdBdb4d16EdA451D0503b854CF79D55697F90c8DF")

  owner = "0x66945268093f28c28A2dcE11a1640e06c070df7B"
  old_vault = "0xa1E263225E24333CA4d26083C94092D6bfEe1DDd"
  await deploy_upgrade(network, dhelper, usdc, old_vault)
  await deploy_pool(network, dhelper, usdc, owner, [AavePool, AlusdPoolV2, BUSDPool, CompoundPool, GUSDPool, TriPool, YPool])
  await change_owner(network, usdc, owner)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
