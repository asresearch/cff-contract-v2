const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20DepositApprover = artifacts.require("ERC20DepositApprover");
const TransferableToken = artifacts.require("TransferableToken")
const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const CFControllerV2 = artifacts.require("CFControllerV2")
const CFVaultV2 = artifacts.require("CFVaultV2")
const UpgradeV1ToV2 = artifacts.require("UpgradeV1ToV2")
const Ownable = artifacts.require("Ownable")
const {DHelper, StepRecorder} = require("./util.js");

var get_target_name = async function(target_token){
  if(target_token == '0x0000000000000000000000000000000000000000')
    return 'eth'
  t = await ERC20Token.at(target_token)
  name = await t.name()
  return name
}
var deploy_vault_and_controller = async function(dhelper, target_token, lp_symbol, network){
  //usdc = "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48";
  name = await get_target_name(target_token)
  sr = new StepRecorder(network, name)
  sr.write("target token", target_token)
  sr.write('name', name)
  crv = "0xD533a949740bb3306d119CC777fa900bA034cd52";
  egap = 5760;

  result = {}
  tlistfactory = await dhelper.readOrCreateContract(TrustListFactory);
  tx = await tlistfactory.createTrustList(['0x0000000000000000000000000000000000000000']);
  tlist = await TrustList.at(tx.logs[0].args.addr);
  sr.write("trustlist", tlist.address)

  tf = await dhelper.readOrCreateContract(ERC20TokenFactory)
  tx = await tf.createCloneToken('0x0000000000000000000000000000000000000000', 0, lp_symbol + " v2", 18, lp_symbol, true, tlist.address);
  lp = await ERC20Token.at(tx.logs[0].args._cloneToken);
  sr.write("CF token", lp.address)

  controller = await dhelper.readOrCreateContract(CFControllerV2, [TransferableToken, AddressArray, SafeMath, Address], crv, target_token, egap)
  sr.write("CF controller", controller.address)

  vault = await dhelper.readOrCreateContract(CFVaultV2, [SafeMath, Address, TransferableToken], target_token, lp.address, controller.address)
  sr.write("CF vault", vault.address)
  await controller.setVault(vault.address)
  await tlist.add_trusted(vault.address)
  return {tlist, lp, vault, controller}
}

var deploy_upgrade = async function(network, dhelper, target_token, old_vault){
  name = await get_target_name(target_token)
  sr = new StepRecorder(network, name)
  nv = sr.value("CF vault")
  up = await dhelper.readOrCreateContract(UpgradeV1ToV2, [TransferableToken], old_vault, nv)
  sr.write("upgrade", up.address)
  sr.write("old vault", old_vault)
}

var deploy_pool = async function(network, dhelper, target_token, owner, pools){
  name = await get_target_name(target_token)
  sr = new StepRecorder(network, name)
  controller = await CFControllerV2.at(sr.value("CF controller"))
  vault = await CFVaultV2.at(sr.value("CF vault"))

  for(let c of pools){
    n = c._json.contractName
    ci = await dhelper.readOrCreateContract(c)
    sr.write(n, ci.address)
    await ci.setController(controller.address, vault.address)
    await controller.add_pool(ci.address)
    await ci.transferOwnership(owner)
  }
}

var change_owner = async function(network, target_token, owner){
  name = await get_target_name(target_token)
  sr = new StepRecorder(network, name)
  v = await Ownable.at(sr.value("trustlist"))
  v.transferOwnership(owner)

  v = await Ownable.at(sr.value("CF token"))
  v.transferOwnership(owner)

  v = await Ownable.at(sr.value("CF controller"))
  v.transferOwnership(owner)

  v = await Ownable.at(sr.value("CF vault"))
  v.transferOwnership(owner)
}
module.exports = {change_owner, deploy_pool, deploy_upgrade, deploy_vault_and_controller}
